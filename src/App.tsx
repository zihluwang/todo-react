import "./App.css"
import { useState } from "react"
import Todo from "./models/Todo.ts"

function App() {
  const [todos, setTodos] = useState<Todo[]>([])

  function addTodo(todo: Todo) {
    setTodos([todo, ...todos])
  }

  function updateTodo(id: number, isFinished: boolean) {
    const updatedTodo = todos.find((todo) => todo.id == id)
    if (updatedTodo) {
      updatedTodo.isFinished = isFinished
    }
    setTodos(todos)
  }

  function updateAllTodos(isFinished: boolean) {
    setTodos(todos.map((todo) => {
      return {...todo, isFinished}
    }))
  }

  function removeAllFinishedTodos() {
    setTodos(todos.filter((todo) => !todo.isFinished))
  }

  return <></>
}

export default App
