import { useState } from "react"
import "./index.css"
import TodoItem from "../TodoItem"
import Todo from "../../models/Todo.ts"

export default function TodoList() {
  const cachedTodos: Todo[] = JSON.parse(localStorage.getItem("todos") ?? "")
  const [todos] = useState<Todo[]>(cachedTodos ?? [])

  function deleteTodo(todoId: string) {
    return () => {
      // todo 删除待办事项
    }
  }

  return (
    <>
      {todos.map((todo) => (
        <TodoItem
          todo={todo}
          deleteTodo={deleteTodo()}
        />
      ))}
    </>
  )
}
