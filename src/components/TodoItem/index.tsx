import DeleteIcon from "@mui/icons-material/Delete"
import { Button } from "@mui/material"
import "./index.css"
import Todo from "../../models/Todo.ts"

interface TodoItemProps {
  todo: Todo
  deleteTodo: () => void
}

export default function TodoItem(todoItemProps: TodoItemProps) {
  const { todo, deleteTodo } = todoItemProps
  return (
    <li className="todo-item-wrapper" key={todo.id}>
      <div className="todo-item">
        <span className="todo-item-title">{todo.title}</span>
        <Button
          variant="contained"
          color="error"
          startIcon={<DeleteIcon />}
          onClick={deleteTodo}>
          删除
        </Button>
      </div>
    </li>
  )
}
