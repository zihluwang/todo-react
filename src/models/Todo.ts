export default interface Todo {
  id: number
  title: string
  isFinished: boolean
}
